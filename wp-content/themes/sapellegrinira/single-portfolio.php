<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package sapellegrinira
 */

get_header();
?>

<main id="primary" class="site-main">
  <div class="content-section ">
    <div class="container p-5">
      <div class="row">
        <div class="col-lg-5 col-sm-12">
          <p>
            <?php the_field('date_project'); ?>
          </p>
          <h1>
            <?php the_field('title_project'); ?>
          </h1>
          <p>
            <?php the_field('description_project'); ?>
          </p>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-5 col-sm-12">
          <!--<a href="#" class="cta">
          <svg width="13px" height="10px" viewBox="0 0 13 10">
            <path d="M1,5 L11,5"></path>
            <polyline points="8 1 12 5 8 9"></polyline>
          </svg>
        </a>-->
          <?php $img_project = get_field('img_project'); if( $img_project ) { ?>
                 <img src="<?php echo $img_project['url']; ?>" alt="" class="card-img-top">
            <?php 	} ?>
          </div>
          <div class="col-lg-4 col-sm-12 mt-3">
            <p>
               <?php the_field('details_project'); ?>
            </p>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12 col-sm-12 mt-5">

          <?php $img_project1 = get_field('img_project1'); if( $img_project ) { ?>
                 <img src="<?php echo $img_project1['url']; ?>" alt="" class="mb-5">
            <?php 	} ?>


          <?php $img_project2 = get_field('img_project2'); if( $img_project ) { ?>
                 <img src="<?php echo $img_project2['url']; ?>" alt="" class="mb-5" >
            <?php 	} ?>

          <?php $img_project3 = get_field('img_project3'); if( $img_project ) { ?>
                 <img src="<?php echo $img_project3['url']; ?>" alt=""  class="mb-5" >
            <?php 	} ?>

          <?php $img_project4 = get_field('img_project4'); if( $img_project ) { ?>
                 <img src="<?php echo $img_project4['url']; ?>" alt="" class="mb-5" >
            <?php 	} ?>

          </div>






      <!--  <div class="col col-lg-6">
          <?php //
             $img_profile_one = get_field('img_profile_one');
             if( $img_profile_one ) { ?>
              <img src="<?php echo $img_profile_one['url']; ?>" alt="" class="w-100">
          <?php 	} ?>
        </div>-->
      </div>

		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', get_post_type() );

			the_post_navigation(
				array(
					'prev_text' => '<span class="nav-subtitle">' . esc_html__( 'Previous:', 'sapellegrinira' ) . '</span> <span class="nav-title">%title</span>',
					'next_text' => '<span class="nav-subtitle">' . esc_html__( 'Next:', 'sapellegrinira' ) . '</span> <span class="nav-title">%title</span>',
				)
			);

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>



	</main><!-- #main -->
</div></div>

<?php

get_footer();
