<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package sapellegrinira
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
	<link rel="icon" href="http://localhost/prova2/wp-content/uploads/2020/09/favicon-1.ico" type="image/x-icon">
</head>

<body <?php body_class(); ?>>

<?php wp_body_open(); ?>
<div id="page" class="site">

<div class="social-ig fixed-top d-none d-lg-block">
				<a href="https://www.instagram.com/sapellegrinira/">
					<i class="fa fa-instagram fa-2x" aria-hidden="true"></i>
				</a>
</div>
<div class="social-fb fixed-top d-none d-lg-block">
				<a href="http://facebook.com/sapellegrinira">
					<i class="fa fa-facebook fa-2x" aria-hidden="true"></i>
				</a>
</div>
<div class="social-be fixed-top d-none d-lg-block">
				<a href="http://linkedin.com/sapellegrinira">
					<i class="fa fa-linkedin fa-2x" aria-hidden="true"></i>
				</a>
</div>


	<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'sapellegrinira' ); ?></a>

	<header id="masthead" class="site-header">
		<nav class="navbar navbar-expand-lg navbar-light navbar-color ">
		<div class="site-branding">
			<?php
			the_custom_logo();
			if ( is_front_page() && is_home() ) :
				?>
				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
				<?php
			else :
				?>

				<!--<p class="site-title"><a href="<?php // echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php // bloginfo( 'name' ); ?></a></p>-->
				<?php
			endif;
			$sapellegrinira_description = get_bloginfo( 'description', 'display' );
			if ( $sapellegrinira_description || is_customize_preview() ) :
				?>
				<!--<p class="site-description"><?php // echo $sapellegrinira_description; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></p>-->
			<?php endif; ?>
		</div><!-- .site-branding -->




		<a class="navbar-brand d-none d-lg-block" href="<?php echo home_url(); ?>"><img width="50px" src="<?php echo get_stylesheet_directory_uri(); ?>/images/Logo_white.png"></a>
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>

	  <div class="collapse navbar-collapse" id="navbarSupportedContent">

		<!--<nav id="site-navigation" class="main-navigation">
			<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php // esc_html_e( 'Primary Menu', 'sapellegrinira' ); ?></button>-->
			<?php
			wp_nav_menu(
				array(
					'theme_location' => 'menu-1',
					'menu_id'        => 'primary-menu',
					'container'			=> 'ul',
					'menu_class'    => 'navbar-nav',
					'add_li_class'  => 'nav-item d-inline-block active',
					'link_class'   	=> 'nav-link'
				)
			);
			?>
		<!--</nav> #site-navigation -->
		</div>
	</nav>

	</header><!-- #masthead -->
