<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package sapellegrinira
 */

?>

	<footer id="colophon" class="site-footer text-center p-3">
		<div class="site-info">
			<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'sapellegrinira' ) ); ?>">
				<?php
				/* translators: %s: CMS name, i.e. WordPress. */
				printf( esc_html__( 'Proudly powered by Sara Pellegrini', 'sapellegrinira' ), 'WordPress' );
				?>
			</a>
			<span class="sep"> | </span>
				<?php
				/* translators: 1: Theme name, 2: Theme author. */
				printf( esc_html__( 'Theme: %1$s by %2$s.', 'sapellegrinira' ), 'sapellegrinira', '<a href="#">Sapellegrinira Portfolio</a>' );
				?>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
