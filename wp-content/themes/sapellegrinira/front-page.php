
  <?php
  /**
   * The front page template file.
   *
   * If the user has selected a static page for their homepage, this is what will
   * appear.
   * Learn more: http://codex.wordpress.org/Template_Hierarchy
   *
   * @package Sarapellegrinira
   * @since Sarapellegrinira
   */


  get_header();
  		/**
  		 * Sapellegrinira Header hook.
  */?>
<div class="container">
<div id="fullpage" >

  <!--SEZIONE UNO -->

    <div class="section s1">
      <div class="content-section">
          <div class="row">
            <div class="col-lg-6 col-sm-12 p-5 testo-sezioni">
              <p><?php the_field('subtitle1_home_s1'); ?>
              </p>
              <h1><?php the_field('title_home_s1'); ?>
              </h1>
              <p><?php the_field('subtitle2_home_s1'); ?>
              </p>
              <br>
              <a href="<?php echo get_page_link( get_page_by_title( 'About Me' )->ID ); ?>" class="cta">
                <span>ABOUT</span>
                <svg width="13px" height="10px" viewBox="0 0 13 10">
                  <path d="M1,5 L11,5"></path>
                  <polyline points="8 1 12 5 8 9"></polyline>
                </svg>
              </a>
            </div>
          <div class="col-lg-6 d-none d-lg-block">
              <?php
                 $img_profile_one = get_field('img_home_s1');
                 if( $img_profile_one ) { ?>
      						<img src="<?php echo $img_profile_one['url']; ?>" alt="" class="w-100 img-fluid" >
      				<?php 	} ?>
            </div>
          </div>
      </div>
    </div>


  <!--SEZIONE DUE -->

    <div class="section s2" >
      <div class="content-section">
          <div class="row">
            <div class="col-lg-6 col-sm-12 mt-5">
              <?php //
                 $img_profile_one = get_field('img_portfolio1');
                 if( $img_profile_one ) { ?>
      						<img src="<?php echo $img_profile_one['url']; ?>" alt="">
      				<?php 	} ?>
            </div>
            <div class="col-lg-5 col-sm-12 p-5">
              <p><?php the_field('subtitle1_home_s2'); ?>
              </p>
              <h1><?php the_field('title_home_s2'); ?>
              </h1>
              <p><?php the_field('subtitle2_home_s2'); ?>
              </p>
              <br>
              <a href="<?php echo get_post_type_archive_link( 'portfolio' ); ?>" class="cta">
                <span>DISCOVER</span>
                <svg width="13px" height="10px" viewBox="0 0 13 10">
                  <path d="M1,5 L11,5"></path>
                  <polyline points="8 1 12 5 8 9"></polyline>
                </svg>
              </a>
            </div>
          </div>
      </div>
    </div>


  <!--SEZIONE TRE -->

    <div class="section s3">
      <div class="content-section">
          <div class="row  ">
            <div class="col-lg-5 testo-sezioni p-5">
              <p><?php the_field('subtitle1_home_s3'); ?>
              </p>
              <h1><?php the_field('title_home_s3'); ?>
              </h1>
              <p><?php the_field('subtitle2_home_s3'); ?>
              </p>
              <br>
              <a href="<?php echo get_page_link( get_page_by_title( 'Contacts' )->ID ); ?>" class="cta">
                <span>CONTACTS</span>
                <svg width="13px" height="10px" viewBox="0 0 13 10">
                  <path d="M1,5 L11,5"></path>
                  <polyline points="8 1 12 5 8 9"></polyline>
                </svg>
              </a>
            </div>

            <div class="col-lg-6 d-none d-lg-block">
              <?php //
                 $img_profile_one = get_field('img_home_s3');
                 if( $img_profile_one ) { ?>
      						<img src="<?php echo $img_profile_one['url']; ?>" alt="" class="w-100">
      				<?php 	} ?>
            </div>

      </div>
    </div>
  </div>

</div>
</div> <!--- chiusura-->

<script type="text/javascript">



  new WOW().init();

  AOS.init();

	new fullpage('#fullpage', {
		//options here
		autoScrolling:true,
		scrollHorizontally: true,
	  navigation: true,
	  anchors: ['section1', 'section2', 'section3'],
	});

</script>
<?php get_footer();?>
