<?php
/**
 * The front page template file.
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Sarapellegrinira
 * @since Sarapellegrinira
 */


get_header();
		/**
		 * Sapellegrinira Header hook.
*/?>

<div id="fullpage">

    <div class="section s1">
      <div class="content-section">
        <div class="container">
          <div class="row">
            <div class="col-lg-6 p-5 col-sm-12">
              <p><?php the_field('subtitle1_about_s1'); ?>
              </p>
              <h1><?php the_field('title_about_s1'); ?>
              </h1>
              <p><?php the_field('subtitle2_about_s1'); ?>
              </p>
              <br>
              <a href="#section2" class="cta" >
              <svg width="13px" height="10px" viewBox="0 0 13 10">
                <path d="M1,5 L11,5"></path>
                <polyline points="8 1 12 5 8 9"></polyline>
              </svg>
              </a>
            </div>

          <div class="col-lg-6 d-none d-lg-block">
              <?php //
                 $img_profile_one = get_field('img_about_s1');
                 if( $img_profile_one ) { ?>
      						<img src="<?php echo $img_profile_one['url']; ?>" alt="" class="w-100">
      				<?php 	} ?>
            </div>
          </div>
      </div>
    </div>
  </div>

    <div class="section s2">
      <div class="content-section">
        <div class="container">
          <div class="row">
            <div class="col-lg-6 col-sm-12 mt-5 px-5">
              <div class="skillbar " data-percent="85%">
                <div class="skillbar-title">ILLUSTRATOR</div>
                <div class="skill-bar-percent">85%</div>
                <div class="skillbar-bar" style="width: 85%"></div>
              </div>
              <div class="skillbar d-none d-lg-block" data-percent="90%">
                <div class="skillbar-title">PHOTOSHOP</div>
                <div class="skill-bar-percent">90%</div>
                <div class="skillbar-bar" style="width: 90%"></div>
              </div>
              <div class="skillbar  d-none d-lg-block" data-percent="80%">
                <div class="skillbar-title">INDESIGN</div>
                <div class="skill-bar-percent">80%</div>
                <div class="skillbar-bar" style="width: 80%"></div>
              </div>
              <div class="skillbar d-none d-lg-block" data-percent="75%">
                <div class="skillbar-title">XD</div>
                <div class="skill-bar-percent">75%</div>
                <div class="skillbar-bar" style="width: 75%"></div>
              </div>
              <div class="skillbar" data-percent="90%">
                <div class="skillbar-title">BRANDING</div>
                <div class="skill-bar-percent">90%</div>
                <div class="skillbar-bar" style="width: 90%"></div>
              </div>
              <div class="skillbar" data-percent="95%">
                <div class="skillbar-title">WEB COMMUNICATION</div>
                <div class="skill-bar-percent">95%</div>
                <div class="skillbar-bar" style="width: 95%"></div>
              </div>
              <div class="skillbar d-none d-lg-block" data-percent="85%">
                <div class="skillbar-title">CSS</div>
                <div class="skill-bar-percent">85%</div>
                <div class="skillbar-bar" style="width: 85%"></div>
              </div>
              <div class="skillbar d-none d-lg-block" data-percent="90%">
                <div class="skillbar-title">HTML</div>
                <div class="skill-bar-percent">90%</div>
                <div class="skillbar-bar" style="width: 90%"></div>
              </div>
            </div>

            <div class="col-lg-5 p-5 col-sm-12">
              <p><?php the_field('subtitle1_about_s2'); ?>
              </p>
              <h1><?php the_field('title_about_s2'); ?>
              </h1>
              <p><?php the_field('subtitle2_about_s2'); ?>
              </p>
              <br>
              <a href="<?php echo get_post_type_archive_link( 'portfolio' ); ?>" class="cta">
                <span>PORTFOLIO</span>
                <svg width="13px" height="10px" viewBox="0 0 13 10" >
                  <path d="M1,5 L11,5"></path>
                  <polyline points="8 1 12 5 8 9"></polyline>
                </svg>
              </a>
            </div>
            </div>
          </div>
        </div>

        </div>


    <div class="section s3">
      <div class="content-section">
        <div class="container">
          <div class="row">
            <div class="col-lg-12 testo-download p-5 col-sm-12">
              <p><?php the_field('subtitle1_about_s3'); ?>
              </p>
              <h1><?php the_field('title_about_s3'); ?>
              </h1>
              <p><?php the_field('subtitle2_about_s3'); ?>
              </p>
              <br>
              <a href="pdf/Pellegrini_Sara_CV.pdf" download class="cta">
                <span>DOWNLOAD</span>
                <svg width="13px" height="10px" viewBox="0 0 13 10">
                  <path d="M1,5 L11,5"></path>
                  <polyline points="8 1 12 5 8 9"></polyline>
                </svg>
              </a>
            </div>

          <!--  <div class="col col-lg-6">
              <?php //
                 $img_profile_one = get_field('img_profile_one');
                 if( $img_profile_one ) { ?>
      						<img src="<?php echo $img_profile_one['url']; ?>" alt="" class="w-100">
      				<?php 	} ?>
            </div>-->
          </div>
      </div>
    </div>
  </div>

</div> <!--- chiusura-->


<script type="text/javascript">

	new fullpage('#fullpage', {
		//options here
		autoScrolling:true,
		scrollHorizontally: true,
	  navigation: true,
	  anchors: ['section1', 'section2', 'section3'],
	});

</script>
<?php get_footer();?>
