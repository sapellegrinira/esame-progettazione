




<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package sapellegrinira
 */

get_header();
?>

<div class="container">
      <div class="row px-3">
        <div class="col-lg-6 col-sm-12 mb-4 animate__animated animate__slideInDown">
          <img src="http://localhost/prova2/wp-content/uploads/2020/09/lavori.png">
        </div>
        <div class="col-lg-6 col-sm-12 my-auto">
          <p>University Projects from 2018
          </p>
          <h1>Enjoy!
          </h1>
          <p>Discover all my works
          </p>
          <br>
          <!--<a href="" class="cta">
            <span>DISCOVER</span>
            <svg width="13px" height="10px" viewBox="0 0 13 10">
              <path d="M1,5 L11,5"></path>
              <polyline points="8 1 12 5 8 9"></polyline>
            </svg>
          </a>-->
        </div>
      </div>

	<main id="primary" class="site-main">

		<?php //if ( have_posts() ) : ?>

			<header class="page-header">

				<?php
				// the_archive_title( '<h1 class="page-title">', '</h1>' );
				// the_archive_description( '<div class="archive-description">', '</div>' );
				?>
			</header><!-- .page-header -->

    <?php

    $query = new WP_Query(array (
        'post_type' => 'portfolio',
        'posts_per_page' => '9',
        'order'   => 'ASC'
    ));
     ?>

  <?php if ( $query->have_posts() ) : ?>

    <!-- apertura fuori dal ciclo -->

<div class="container">
  <div class="row ">



            <?php
            /* Start the Loop  articoli dell'archivio/immagine titolo*/

            while ( $query->have_posts() ) : $query->the_post();

              // Imposto la variabile dentro il while

              $title_project = get_field('title_project');
              $description_project = get_field('description_project');
              $date_project = get_field('date_project');
            ?>

                <div class="card-deck animate__animated animate__zoomIn col-lg-4 col-sm-12 mb-4">
                  <a href="<?php the_permalink(); ?>" >
                  <div class="card text-white bg-dark wow fadeInDown">
                    <div class="img-card">
                      <?php $img_project = get_field('img_project'); if( $img_project ) { ?>
                             <img src="<?php echo $img_project['url']; ?>" alt="" class="card-img-top">
                				<?php 	} ?>
                    </div>
                    <div class="card-body">
                      <h5 class="card-title"><?php echo $title_project ?></h5>
                      <p class="card-text">
                        <?php echo $description_project ?>
                      </p>
                    </div>
                    <div class="card-footer">
                      <small class="text-muted"><?php echo $date_project ?></small>
                    </div>
                  </div>
                </div>
              </a>




              <?php
                endwhile;
                the_posts_navigation();
              endif;
              wp_reset_postdata();
              ?>
               </div>
             </div>
              </div>

            </div>
        </div><!-- end hero-text -->
      </div>
    </div>
	</main><!-- #main -->
</div>
<?php

get_footer();
